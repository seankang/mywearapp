package com.seankang.mywearapp


import android.os.AsyncTask
import org.jsoup.Jsoup
import org.jsoup.nodes.Element


class DownloadStockPriceTask()  : AsyncTask<String, Integer, String>() {



    override fun doInBackground(vararg myurl: String?): String {
        var output = ""
        Jsoup.connect(myurl.toString()).get().run {
            //2. Parses and scrapes the HTML response
            select("div#quote-header-info").forEachIndexed { index, element ->

                val child: Element = element.child(2)
                val child2 = child.child(0)
                val child3 = child2.child(0)
                val html = child3.html()
                println(html)
                output = html

            }
        }

        return output
    }


    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

    }

}