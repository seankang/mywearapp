package com.seankang.mywearapp

import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.widget.Button
import android.widget.TextView


class MainActivity : WearableActivity() {


    private var textResult : TextView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Enables Always-on
        setAmbientEnabled()
        textResult = findViewById(R.id.editTextStockPrice)


        val buttonLoad = findViewById(R.id.buttonFetch) as Button
        buttonLoad.setOnClickListener{
            loadData()
        }
    }

    fun loadData() {
        print("loading data")
        val url = "https://finance.yahoo.com/quote/AAPL?p=AAPL&.tsrc=fin-srch"
        DownloadStockPriceTask().execute(url)
    }
}
